Class name: FileCreator.java
Parent class: None
Subclasses: None
Responsibility:
	- Creates, overrides and overloads files
	- Stores and overloads outputs of commands inside new or existing files
Collaborators:
	- FileSystem