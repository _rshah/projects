The purpose of this project was to create a Linux based terminal using Java. It was a team project, used SVN to efficiently share code between members. In addition, we followed Single responsibility principle to make the project look clean and easy to read. Few of the commands we implemented were mkdir, ls, cd, pwd, cp, etc.

Language used was Java.
