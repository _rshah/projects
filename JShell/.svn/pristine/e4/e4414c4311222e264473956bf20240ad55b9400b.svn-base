// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//

package Commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import Helper.*;
import driver.*;

/**
 * 
 * @author Spasimir Vasilev
 *
 */
public class LS extends Commands {
  public LS(String userInput) {
    super.setInput(userInput); // super is the Command class
  }

  public String execute(FileSystem fs) throws Exception {
    if (getArguments().length == 0) {
      return fs.getContent().trim(); // if the length is zero, get the content
    } else if (getArgumentAtPosition(0).equals("-R")
        && getArguments().length == 1) {
      return recursiveList(fs, ".").trim();
    } else if (getArgumentAtPosition(0).equals("-R")) {
      String result = "";
      for (String path : Arrays.copyOfRange(getArguments(), 1,
          getArguments().length))
        try {
        result += recursiveList(fs, path);
        } catch(Exception e){
          System.out.println(path + " does not exist");
        }
      return result.trim();
    } else {
      String result = "";
      for (String path : Arrays.copyOfRange(getArguments(), 0,
          getArguments().length))
        try{
        result += nonRecursiveList(fs, path);
        } catch (Exception e){
          throw new Exception(e.getMessage());
        }
      return result.trim();
    }
  }

  private String recursiveList(FileSystem fs, String path) throws Exception {
    String resultant = "";
    fs = getPathHandler().toThePath(fs, path.split("/"), path.startsWith("/"));
    if (fs.getAllChildrenName() == null) {
      return "";
    } else if (fs.getChildrenFS() == null) {
      return nonRecursiveList(fs, ".");
    } else {
      resultant += nonRecursiveList(fs, ".");
      for (String dirName : fs.getChildrenFS().keySet()) {
        try {
        resultant += recursiveList(fs, dirName);
        } catch(Exception e){
          System.out.println(e.getMessage());
        }
      }
      return resultant;
    }
  }

  /**
   * Display the content of the file in the form of a String
   *
   * @param fs file system object
   * @param pathHandler pathHandler object
   * @throws Exception 
   */
  private String nonRecursiveList(FileSystem fs, String pathName) throws Exception {
    String dirContent = "";
    String[] path;
    path = createNewPath(pathName.split("/"));
    if (path.length > 0) {
      dirContent += listContent(fs, getPathHandler(), path, pathName);
    } else {
      dirContent += "\nDirectory \"" + "/" + "\" has\n"
          + getPathHandler()
              .toThePath(fs, new String[] {}, pathName.startsWith("/"))
              .getContent();
    }
    return dirContent;
  }


  /**
   * List the content of the directories (i.e the within directories)
   *
   * @param fs file system object
   * @param pathHandler pathHandler object
   * @return a string of the contents
   * @throws Exception 
   */
  private String listContent(FileSystem fs, PathHandler pathHandler,
      String[] path, String argument) throws Exception {
    String dirName = path[path.length - 1];
    fs = pathHandler.toThePath(fs, Arrays.copyOfRange(path, 0, path.length - 1),
        argument.startsWith("/"));
    if (fs == null || !fs.getAllChildrenName().contains(dirName)
        && (!dirName.equals(".") && !dirName.equals(".."))) {
      // The other directories, if more than one arguments
      throw new Exception(
          "\nThe Dir \"" + path[path.length - 1] + "\": does not exist");
    } else if (dirName.equals(".")) {
      return "\nDirectory \"" + fs.getName() + "\" has\n" + fs.getContent();
    } else if (dirName.equals("..") && fs.getParent() != null) {
      fs = fs.getParent();
      return "\nDirectory \"" + fs.getName() + "\" has\n" + fs.getContent();
    } else if (fs.getChildrenFile().containsKey(dirName)) {
      return dirName;
    } else if (fs.getChildrenFS().containsKey(dirName)) {
      fs = fs.getChildrenFS().get(dirName);
      return "\nDirectory \"" + fs.getName() + "\" has\n" + fs.getContent();
    }
    return "";
  }

  /**
   * Create s new directory path
   *
   * @param path user path input
   * @return a list of string
   */
  private String[] createNewPath(String[] path) {
    List<String> listPaths = new ArrayList<String>();
    for (String pathName : path) { // Go through the path
      if (!pathName.equals(""))
        // add the path names into an array
        listPaths.add(pathName);
    }
    String[] newPaths = new String[listPaths.size()];
    newPaths = listPaths.toArray(newPaths);
    return newPaths;
  }
}
