// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//

package Commands;

import java.util.Arrays;
import Helper.*;
import driver.*;

/**
 * @author Anees Bajwa
 *
 */
public class Echo extends Commands {

  /**
   * class constructor
   */
  public Echo(String userInput) {
    super.setInput(userInput); // set commands by the super class method
  }

  /**
   * set the string of file content to be added to file
   * 
   * @param newInput a string from user
   */
  public void setInput(String userInput) {
    setCommand("echo"); // Set command
    setArgument(new String[] {"", "", ""}); // set arguments
    String newInput = userInput.trim();
    int initIndex;
    int lastIndex = 0;
    if (newInput.length() - newInput.replace("\"", "").length() > 1
        && newInput.startsWith("\"")) { // check quotation marks for file's
                                        // content
      initIndex = newInput.indexOf("\"") + 1;
      lastIndex = newInput.lastIndexOf("\"");
      newInput = newInput.substring(initIndex, lastIndex);
    } else { // if we do not have quotation marks surrounding around contents
      newInput = newInput.substring(4).trim();
      initIndex = 0;
      if (newInput.contains(">>")) {
        lastIndex = newInput.lastIndexOf(">>");
      } else if (newInput.contains(">")) {
        lastIndex = newInput.lastIndexOf(">");
      } else {
        lastIndex = newInput.length();
      }
      newInput = newInput.substring(initIndex, lastIndex).trim();
    }
    if (lastIndex == newInput.length()) { // if echo has 1 element set 1 element
      setArgument(new String[] {newInput});
    } else { // set 3 elements for echo
      setArgumentAtPosition(0, newInput);
      setArgumentAtPosition(1,userInput.split(" +")[userInput.split(" +").length - 2]);
      setArgumentAtPosition(2,userInput.split(" +")[userInput.split(" +").length - 1]);
    }
  }

  /**
   * Decide if it is going to append it to a file, overwrite, or print it
   *
   * @param fs a file system object
   * 
   * @param pathHandler PathHandler object
   */
  public String execute(FileSystem fs) {
    if (getArguments().length == 1) {
      return getArgumentAtPosition(0);
    } else if (getArgumentAtPosition(1).equals(">")) {
      overWriteFile(fs, getPathHandler());
    } else if (getArgumentAtPosition(1).equals(">>")) {
      apppendFile(fs, getPathHandler());
    }
    return "";
  }

  /**
   * OverWrite the file
   *
   * @param fs a file system object
   * @param pathHandler PathHandler object
   */
  private void overWriteFile(FileSystem fs, PathHandler pathHandler) {
    String[] paths = getArgumentAtPosition(2).split("/"); // Splits the string
    String fileName = paths[paths.length - 1]; // Grabs the filename
    FileSystem fsClone = pathHandler.toThePath(fs,
        Arrays.copyOfRange(paths, 0, paths.length - 1), // Creates a FileSystem
        getArgumentAtPosition(2).startsWith("/"));
    // If the FS contains the file
    if (fsClone == null) {
      System.out.println("File is invalid");
      return;
    } else if (fsClone.getChildrenFile().containsKey(fileName)) {
      // Then you want to add the contents
      fsClone.getChildrenFile().get(fileName).addContent(true, getArgumentAtPosition(0));
    } else {
      if (!fsClone.getChildrenFS().containsKey(fileName))
        // If not, you want to add the file that not conflicts with FileSystem
        // Name
        fsClone.addFile(fileName, true, getArgumentAtPosition(0));
      else {
        System.out.println("The file name \"" + fileName + "\" is invalid");
      }
    }
  }

  /**
   * Append to the file
   *
   * @param fs a file system object
   * @param pathHandler PathHandler object
   */
  private void apppendFile(FileSystem fs, PathHandler pathHandler) {
    String[] paths = getArgumentAtPosition(2).split("/"); // Split the string
    String fileName = paths[paths.length - 1];
    FileSystem fsClone = pathHandler.toThePath(fs,
        Arrays.copyOfRange(paths, 0, paths.length - 1), // Make FileSystem
        getArgumentAtPosition(2).startsWith("/"));
    // If the FileSystem contains the file just append it to the file, if not
    // make the file
    if (fsClone.getAllChildrenName().contains(fileName)) {
      fsClone.getChildrenFile().get(fileName).addContent(false, getArgumentAtPosition(0));
    } else {
      fsClone.addFile(fileName, false, getArgumentAtPosition(0));
    }
  }
}
