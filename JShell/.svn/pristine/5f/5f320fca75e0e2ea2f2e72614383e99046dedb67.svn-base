// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
// *********************************************************
package driver;

import java.util.Hashtable;
import java.util.Scanner;
import Helper.*;
import Commands.*;

/**
 * @author Raj
 *
 */
public class JShell {
  public static FileSystem system; // current working directory
  static DirectoryStack dir; // Stack of all directories
  static Validate validate; // Validate if path exists
  static Scanner user = new Scanner(System.in); // User input
  static Commands userCommand;
  static Log history; // stores the history of user inputs
  static Hashtable<String, Commands> listOfCommands; // All commands
  static Exclamation commandRecheck;

  /**
   * Initialize all the classes
   *
   */
  private static void Initialize() {
    history = new Log();
    system = new FileSystem("/");
    dir = new DirectoryStack();
    validate = new Validate();
    user = new Scanner(System.in);
    userCommand = new Commands();
    listOfCommands = addCommands();
    commandRecheck = new Exclamation();
  }

  public static void main(String[] args) throws Exception {
    boolean loop = true;
    Initialize();
    while (loop) {
      System.out.print("/# ");
      String command = commandRecheck.execute(history, user.nextLine());
      if (command != null) {
        history.setNewInput(command);
        userCommand.setInput(command);
        String commandName = userCommand.getCommand();// Command name
        if (validate.argChecker(system, commandName,
            userCommand.getArguments())) {
          if (commandName.equals("popd") || commandName.equals("pushd")
              || commandName.equals("cd")) { // If its one of than, than change
                                             // path
            if (changePath(command, commandName) != null) {
              system = changePath(command, commandName);
            }
          } else if (listOfCommands.containsKey(commandName)) {
            execute(command, commandName);
          } else if (commandName.equals("exit")) { // Stop the loop
            loop = false;
          } else {
            System.out.println("error");
          }
        } else
          System.out.println("Invalid command, Please try again");
      }
    }
  }

  /**
   * execute the execute method from each class
   *
   * @param
   * @param commandName the user command enters
   * @param argumentList the list of arguments
   * @throws Exception
   */
  private static void execute(String command, String commandName)
      throws Exception {
    Commands main = listOfCommands.get(commandName);
    main.setInput(command);
    String executer;
    if (main.getCommand().equals("history")
        || main.getCommand().equals("Fact")) {
      executer = main.execute(history);
    } else {
      executer = main.execute(system); // Get this from the method
    }
    if (!executer.equals(""))
      System.out.println(executer);
  }

  /**
   * Used for Change directory, and the other like pushd and popd
   *
   * @param command whole user input
   * @param commandName just the name of the command
   * @return a file system object
   */
  private static FileSystem changePath(String command, String commandName) {
    CD cd = new CD(command);
    if (commandName.equals("pushd")) {
      dir.add(system);
    } else if (commandName.equals("popd")) {
      String[] newPath = new String[] {dir.remove()};
      if (newPath[0] != null) {
        cd.setArgument(newPath);
      } else
        return null;
    }
    FileSystem newPath = cd.changPath(system);
    return newPath;
  }

  /**
   * Returns the hastable of the string as key and the object of class as value
   *
   * @return hashtable of string and each corresponding class
   */
  private static Hashtable<String, Commands> addCommands() {
    Hashtable<String, Commands> listOfCommands =
        new Hashtable<String, Commands>();
    listOfCommands.put("mkdir", new MkDir(""));
    listOfCommands.put("cd", new CD(""));
    listOfCommands.put("ls", new LS(""));
    listOfCommands.put("pwd", new PWD(""));
    listOfCommands.put("pushd", new CD(""));
    listOfCommands.put("popd", new CD(""));
    listOfCommands.put("cat", new Cat(""));
    listOfCommands.put("echo", new Echo(""));
    listOfCommands.put("man", new Manual(""));
    listOfCommands.put("history", new History(""));
    listOfCommands.put("mv", new MV(""));
    listOfCommands.put("get", new Get(""));
    listOfCommands.put("cp", new Cp(""));
    return listOfCommands;
  }
}


