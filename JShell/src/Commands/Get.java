// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
// *********************************************************


package Commands;

import Helper.Commands;
import java.net.*;
import java.io.*;
import driver.*;


/**
 * 
 * @author Spasimir
 *
 */
public class Get extends Commands {
  private String[] address;
  private String name;
  private URL web;
  private URLConnection yc;
  private BufferedReader in;
  private String content;
  private String inputLine;
  private String[] newAddress;

  //Takes in the user input
  public Get(String userInput) {
    super.setInput(userInput);
  }

  /**
   * Grabs the url text file and stores into a file with the same name
   * 
   * @param fs is a filesystem object
   * @throws Exception
   * @return empty string, cause of polymorphism
   */
  public String execute(FileSystem FS) throws Exception {
    try{ //Try block
    address = getArguments(); //Get the arguments from parent class
    newAddress = address[0].split("/"); //Split it at slashes 
    name = newAddress[newAddress.length-1];
    web = new URL(address[0]); //URL object
    yc = web.openConnection();
    in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
    content = new String();
    while ((inputLine = in.readLine()) != null) //Go through each line
      content += inputLine + "\n"; //Put all the content in a file
    in.close();

    if (FS.isFileExist(name)) { //If a file exist with the same name 
      FS.addFile(name, true, content);
    } else { //if not
      FS.addFile(name, false, content);
    }
    return null;
    }
    catch (Exception e){ //Exception if the url does not exist
      throw new Exception(e.getMessage() + " does not exist");
    }

  }



}
