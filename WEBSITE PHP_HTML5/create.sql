drop table iGetit;
drop table taughtClasses;
drop table takenClasses;
drop table information;
drop table appuser;

create table appuser( 
	username varchar(20) primary key, 
	password varchar(20),
	times INT
	 
);
create table information (
	username varchar(20) REFERENCES appuser,
	first varchar(20),
	last varchar(20),
	email varchar(30) PRIMARY KEY,
	role varchar(20)
);

create table taughtClasses (
	username varchar(20) REFERENCES appuser,
	class varchar(20) PRIMARY KEY,
	code varchar(20)
);

create table takenClasses (
	username varchar(20) REFERENCES appuser,
	class varchar(20) REFERENCES taughtClasses,
	code varchar(20) 
);

create table iGetit (
	username varchar(20) REFERENCES appuser,
	getIt varchar(20),
	class varchar(20) REFERENCES taughtClasses,
	time DATE	
);
 
