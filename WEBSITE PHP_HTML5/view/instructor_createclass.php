<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="style.css" />
		<title>iGetIt</title>
	</head>
	<body>
		<header><h1>iGetIt (instructor)</h1></header>
		<nav>
			<ul>
                        <li> <a>Class</a>
                        <li> <a href="index.php?profile=true">Profile</a>
                        <li> <a href="index.php?logout=true">Logout</a>
                        </ul>
		</nav>
		<main>
			<h1>Class</h1>
			<form>
				<fieldset>
					<legend>Create Class</legend>
   					<p> <label for="class">class</label><input type="text" name="cclass"></input> </p>
   					<p> <label for="code">code</label><input type="text" name="code"></input> </p>
                                        <p> <input type="submit" name="create" value="create" />
				</fieldset>
			</form>
 			<form method="post">
                                <fieldset>
                                        <legend>Current Classes</legend>
                                        <select name="theClass">
                                        <?php
                                        	while ($row = pg_fetch_array($_SESSION['results'])) {
                                        		$className=$row["class"];
                                        		$firstName=$row["first"];
                                        		$lastName=$row["last"];
                                        	 	echo "<option name='option' value=". "$className" . ">" . "$className" . " " . "$firstName" . " ". "$lastName" ."</option>";
                                        	 }
                                        ?>
                                        </select>
                                        <p> <input type="submit"  name="Enter" value="Enter" />
                                </fieldset>
                        </form>

		</main>
		<footer>
		</footer>
	</body>
</html>

