<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="style.css" />
		<style>
			td a {
				background-color:green; 
				display:block; 
				width:200px; 
				text-decoration:none; 
				padding:20px; 
				color:white; 
				text-align:center;
			}
		</style>
		<title>iGetIt</title>
	</head>
	<body>
		<header><h1>iGetIt (student)</h1></header>
		<nav>
			<ul>
                        <li> <a href="index.php?class=true">Class</a>
                        <li> <a href="index.php?profile=true">Profile</a>
                        <li> <a href="index.php?logout=true">Logout</a>
                        </ul>
		</nav>
		<main>
			<h1>Class</h1>
			<form>
				<fieldset style="width: 0%">
					<legend align= "center"> <?php echo $_SESSION['className'] . " " . $_SESSION['firstI'] . " " . $_SESSION['lastI']; ?> </legend>
					<table style="width:0%">
						<tr>
							<td><a style="background-color:#1EAAC2;" href="index.php?getit=true">i Get It</a></td>
							<td><a style="background-color:#EA4E4E;  " href="index.php?dgetit=true">i Don't Get It</a></td>
						</tr>
					</table>
				</fieldset>
			</form>
		</main>
		<footer>
		</footer>
	</body>
</html>

