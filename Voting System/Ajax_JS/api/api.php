<?php
// get the HTTP method, path and body of the request
$method = $_SERVER['REQUEST_METHOD'];
$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
$input = json_decode(file_get_contents('php://input'),true);
$dbconn = pg_connect("host=mcsdb.utm.utoronto.ca dbname=shahraj_309 user=shahraj password=78117");

if(!$dbconn){
	echo "Error Connecting to database, try again laster";
}

$check = $input['check'];
if ($method == "POST") {
	if($check == "login") { //if they trying to login, they check user and pass in database
		$query = pg_prepare($dbconn, "qlogin", "SELECT username, password FROM euser WHERE username=$1 and password=$2"); # check result
	 	$query = pg_execute($dbconn, "qlogin", array($input['user'], $input['pass']));
		$arrayresult = pg_fetch_array($query);

		if(!empty($arrayresult)) {
			echo json_encode("pass");
		}
		else {
			echo json_encode("please try again");
		}
	}
	else if($check == "getvote") {
			$top0 = pg_prepare($dbconn, "vote0", "SELECT COUNT(username) as votecount FROM evote WHERE vote=0");
			$top0 = pg_execute($dbconn, "vote0", array());
			$all = pg_fetch_array($top0);
			$name[] = array("user" => "No", "highscore" => $all["votecount"]);  //ADD THE FIRST ELEMENT OF ARRAY (user:no, highscore:count)

			$top1 = pg_prepare($dbconn, "vote1", "SELECT COUNT(username) as votecount FROM evote WHERE vote=1");
			$top1 = pg_execute($dbconn, "vote1", array());
			$all = pg_fetch_array($top1);
			$name[] = array("user" => "Yes", "highscore" => $all["votecount"]); //ADD IT TO THE BACK
			echo json_encode($name);
		}
	else if($check == "vote") { //If the user picks a vote
		$query = pg_prepare($dbconn, "qvote", "SELECT username FROM evote WHERE username=$1"); # check result
		$query = pg_execute($dbconn, "qvote", array($input['user']));
		$arrayresult = pg_fetch_array($query);

		if($input['answer'] == "yes") { //If they want an extension, it checks if the user exist, if it does, do update else insert

		if(!empty($arrayresult)) {
			$query = pg_prepare($dbconn, "qvotupd", "UPDATE evote set vote=1 WHERE username=$1"); # check result
		 	$query = pg_execute($dbconn, "qvotupd", array($input['user']));
			echo json_encode("updated yes");

		}
		else {
			$query = pg_prepare($dbconn, "qins", "INSERT INTO evote values($1, 1)"); # check result
			$query = pg_execute($dbconn, "qins", array($input['user']));
				echo json_encode("inserted yes");
		}
	}
	else { //if user dont want it
		if(!empty($arrayresult)) {
			$query = pg_prepare($dbconn, "qvotupd", "UPDATE evote set vote=0 WHERE username=$1"); # check result
			$query = pg_execute($dbconn, "qvotupd", array($input['user']));
				echo json_encode("updated no");

		}
		else {
			$query = pg_prepare($dbconn, "qins", "INSERT INTO evote values($1, 0)"); # check result
			$query = pg_execute($dbconn, "qins", array($input['user']));
				echo json_encode("inserted no");
		}
	}
}
	}

?>
