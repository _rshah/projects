var MongoClient = require('mongodb');
var url = 'mongodb://shahraj:78117@mcsdb.utm.utoronto.ca:27017/shahraj_309'

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var regcheck = 0;
var vote0 = 0;
var vote1 = 0;

app.use('/', express.static('static_files'));
app.use(bodyParser.json());

app.get('/users/', function(req, res) { //Login
	  MongoClient.connect(url, function(err, db) {
        if (!err) {
            var user = db.collection('users');
            user.find().toArray(function(err, data) {
              if (err) {
                  res.send("Error with data");
              } else if (data.length) {
                  for (var i = 0; i < data.length; i++) {  //LOOP THROUGH COLLECTION
                      if (data[i]["username"] == req.query.user && data[i]["password"] == req.query.pass) {
                          regcheck = 1; //IF FOUND!!
                          break;
                      }
                  }
                  if (regcheck == 1) {
                      res.send("pass");
                      regcheck = 0;
                      db.close();
                  } else {
                      res.send("please try again");
                      regcheck = 0;
                      db.close();
                  }
              }
          });
        }
    });
});

app.get('/vote/', function(req, res) { //Login
	  MongoClient.connect(url, function(err, db) {
        if (!err) {
            var votetable = db.collection('vote');
            votetable.find().toArray(function(err, data) {
              if (err) {
                  res.send("Error with data");
              } else if (data.length) {
                  for (var i = 0; i < data.length; i++) {  //LOOP THROUGH COLLECTION
                      if (data[i]["username"] == req.query.vuser) { //uses keys to find
                          regcheck = 1; //IF FOUND!!
                          break;
                      }
                  }
                  if (regcheck == 0) {
										var documentt = {username: req.query.vuser, vote: req.query.answer};
				          	votetable.insert(documentt, function(err, records) {
											if (err) throw err;
										});
                    res.send("pass"); //have to send something, and make var back to 0
                    regcheck = 0;
                    db.close();
                  } else {
											var documentt = {vote: req.query.answer}; //UPDATING
											votetable.update({username: req.query.vuser}, {$set: documentt}, function(err, results) {
												if (err) throw err;
											});
                      res.send("update");
                      regcheck = 0;
                      db.close();
                  }
              }
          });
        }
    });
});

app.get('/getvote/', function(req, res) { //Login
	  MongoClient.connect(url, function(err, db) {
        if (!err) {
            var votet = db.collection('vote');
            votet.find().toArray(function(err, data) {
              if (err) {
                  res.send("Error with data");
              } else if (data.length) {
                  for (var i = 0; i < data.length; i++) {  //LOOP THROUGH COLLECTION
											console.log(data[i]);
                      if (data[i]["vote"] == "0") { //IF VOTE IS 0
                          vote0 += 1;
                      }
											else {
												vote1 += 1;
											}
                  }
                  var tarray = []; //TO SEND A LIST U HAVE TO PUSH, NOT STRAIGHT UP
									tarray.push(vote0);
									tarray.push(vote1);
									res.send(tarray);
									vote0 = 0; //Reinitialize
									vote1 = 0;
									db.close();
              }
          });
        }
    });
});

app.listen(10750, function () { //ORIGINAL PORT
  console.log('Example app listening on port 10750!');
});
