#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.util import irange,dumpNodeConnections
from mininet.log import setLogLevel

class CustomTopo(Topo):
    def __init__(self,linkopts1, linkopts2, linkopts3, fanout=2, **opts):
        Topo.__init__(self, **opts)
        self.linkopts1 = linkopts1
        self.linkopts2 = linkopts2
        self.linkopts3 = linkopts3
        self.fanout = fanout

        #create core switch
        c = self.addSwitch('c1')
        #hold aggregation layer
        a_switches = []
        #hold edge layer
        e_switches = []

        #create aggregation switches
        for i in range(1,self.fanout+1):
            a = self.addSwitch('a%s' % i)
            a_switches.append(a)
            self.addLink(a, c, bw=linkopts1['bw'], delay=linkopts1['delay'])

        #create edge switches
        a_switch_ind=0
        num1 = 0
        while a_switch_ind < len(a_switches):
            for i in range(1,self.fanout+1):
                e = self.addSwitch('e%s' % str(num1+1))
                e_switches.append(e)
                self.addLink(e, a_switches[a_switch_ind],bw=self.linkopts2['bw'], delay=linkopts2['delay'])
                num1+=1

            a_switch_ind+=1

        #Make hosts
        e_switch_ind = 0
        num=0
        while e_switch_ind < len(e_switches):
            for i in range(1, self.fanout+1):
                h = self.addHost('h%s' % str(num+1))
                self.addLink(h, e_switches[e_switch_ind], bw=self.linkopts3['bw'], delay=self.linkopts3['delay'])
                num+=1
            e_switch_ind+=1



def simpleTest():
    linkopts1 = {'bw':50, 'delay':'5ms'}
    linkopts2 = {'bw':30, 'delay':'10ms'}
    linkopts3 = {'bw':10, 'delay':'15ms'}
    topo = CustomTopo(linkopts1, linkopts2, linkopts3, fanout=2)
    net = Mininet(topo, link=TCLink)
    net.start()
    print "Pinging all hosts"
    net.pingAll()
    print "Testing bandwidth between h4 and h8"
    h4, h8 = net.get('h4', 'h8')
    net.iperf((h4, h8))
    net.stop()

if __name__ == '__main__':
    # Tell mininet to print useful information
    setLogLevel('info')
    simpleTest()
