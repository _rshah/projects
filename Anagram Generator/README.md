The purpose of this project was to create an anagram solver that would use a word list to find all combinations of words that use same letter or phrase. This was implemented with recursion and classes. 

Language used was Python.
