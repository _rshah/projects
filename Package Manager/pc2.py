import sys

def matrixMaker(array2d, total):
	matrix = []
	tempLst = []
	
	for index in range(total):
		matrix.append([])
		
	for index in range(len(array2d)):
		for i in range(len(array2d[index])):
			temp = array2d[index][i]
			for index2 in range(len(array2d)):
				if temp in array2d[index2]:
					tempLst.append(1)
				else:
					tempLst.append(0)
			matrix[temp-1] = tempLst
			tempLst = []

	for index in range(len(matrix)):
		if (len(matrix[index]) == 0):
			for i in range (len(array2d)):
					tempLst.append(0)
			matrix[index] = tempLst
			tempLst = []
	
	return matrix

def dependencies_finder(matrix,total):
	indeg = []
	flag = []
	answer = ""
	k = 0
	
	for i in range(total):
		indeg.append(0)
		flag.append(0)
	
	for i in range(total):
		for j in range(total):
			indeg[i] = indeg[i] + matrix[j][i]
	
	while (k < total):
		if((indeg[k] == 0) and (flag[k] == 0)):
			flag[k] = 1
			answer += str(k+1) + " "
			for p in range(total):
				if matrix[k][p] == 1:
					matrix[k][p] = 0
					indeg[p] = indeg[p] - 1
			k = -1
		k += 1
	print(answer)
	
if __name__ == "__main__":
	lst = []
	f = sys.stdin
	line = f.readline().strip()
	linelst = [line.split()]
	total = int(linelst[0][0])
	array2d = []

	while line != "":
		line = f.readline().strip()
		if (line != ""):
			linelst.append(line.split())
	
	for index in range(total):
		array2d.append([])
	
	for j in range(len(linelst)):
		array2d[int(linelst[j][0])-1] = list(map(int, linelst[j][2:]))
	
	matrix = matrixMaker(array2d, total)
	dependencies_finder(matrix, total)
